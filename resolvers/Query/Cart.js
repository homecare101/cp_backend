const { AuthenticationError, UserInputError } = require('apollo-server');

const mongoose = require('mongoose');

const { UserSchema, CartSchema, CaseSchema, OrderSchema} = require('../../schema/index')

 const getOrderData = (
    _,
    user
	) => {

    return new Promise (
        (resolve, reject) => {
        const Order = mongoose.model('Order', OrderSchema);
        const User = mongoose.model('User', UserSchema);
        if (!user.userId) {
            reject(new UserInputError(`사용자 아이디를 입력해주세요`))
            return
        }
        User.findOne({
            _id: user.userId
        },
        (err, existingUser) => {
            if(existingUser){
                Order.find({
	                userId: user.userId
	            },
	            (err, existingOrders) => {
	                	resolve ({
	                		order : existingOrders
	                	})
	            })     
            } else {
                 reject(new AuthenticationError(`Please send valid userId`))
            }
         })

 		}
    )
}

const getAllOrders = (
    _,
    user
	) => {

    return new Promise (
        (resolve, reject) => {
        const Order = mongoose.model('Order', OrderSchema);
        const User = mongoose.model('User', UserSchema);
        if (!user.userId) {
            reject(new UserInputError(`사용자 아이디를 입력해주세요`))
            return
        }
        User.findOne({
            _id: user.userId
        },
        (err, existingUser) => {
            if(existingUser && existingUser.role === "Admin"){
                Order.find({
	               
	            },
	            (err, existingOrders) => {
	                	resolve ({
	                		order : existingOrders
	                	})
	            })     
            } else {
                 reject(new AuthenticationError(`해당 권한이 없습니다`))
            }
         })

 		}
    )
}

const changeOrderStatus= (
    _,
    orderDetail
	) => {

    return new Promise (
        (resolve, reject) => {
        const Order = mongoose.model('Order', OrderSchema);
        const User = mongoose.model('User', UserSchema);
        if (!orderDetail.userId) {
            reject(new UserInputError(`사용자 아이디를 입력해주세요`))
            return
        }
        User.findOne({
            _id: orderDetail.userId
        },
        (err, existingUser) => {
            if(existingUser && existingUser.role === "Admin"){
                Order.findByIdAndUpdate(
                    orderDetail.orderId,
                    {
                        $set: {
                            status: orderDetail.status
                        }
                    },
                    () => {
                        resolve({
                            message: "주문이 수정되었습니다"
                        })
                    },
                    (error) =>{
                        reject(new AuthenticationError(`해당 권한이 없습니다`))
                    }
                )   
            } else {
                 reject(new AuthenticationError(`해당 권한이 없습니다`))
            }
         })

 		}
    )
}

const getCartData = (
    _,
    user
    ) => {

    return new Promise (
        (resolve, reject) => {
        const Cart = mongoose.model('Cart', CartSchema);
        const User = mongoose.model('User', UserSchema);
        const Case = mongoose.model('Case', CaseSchema);
        if (!user.userId) {
            reject(new UserInputError(`사용자 아이디를 입력해주세요`))
            return
        }
        User.findOne({
            _id: user.userId
        },
        (err, existingUser) => {
            if(existingUser){
               Cart.find({
                    userId: user.userId
                },
                (err, existingOrders) => {
                        resolve (existingOrders)
                })     
            } else {
                 reject(new AuthenticationError(`Please send valid userId`))
            }
         })

        }
    )
}


module.exports = {
    getOrderData,
    getCartData,
    getAllOrders,
    changeOrderStatus
}