const { AuthenticationError, UserInputError } = require('apollo-server');

const mongoose = require('mongoose');
const { UserSchema, CaseSchema, StockCasesSchema, ModelSchema } = require('../../schema/index')

let colors = [
    {
        _id: 1,
        label: 'Black',
        value: 'black',
        code: '#434243'
    },
    {
        _id: 2,
        label: 'Grey',
        value: 'grey',
        code: '#ededed'
    },
    {
        _id: 3,
        label: 'Pink',
        value: 'pink',
        code: '#ebcec7'
    },
    {
        _id: 4,
        label: 'Red',
        value: 'red',
        code: '#ff551d'
    },
    {
        _id: 5,
        label: 'Sky Blue',
        value: 'skyBlue',
        code: '#59cbff'
    },
    {
        _id: 6,
        label: 'Sea green',
        value: 'seaGreen',
        code: '#44f0ca'
    },
    {
        _id: 7,
        label: 'Brown',
        value: 'brown',
        code: '#6f4a29'
    },
    {
        _id: 8,
        label: 'Blue',
        value: 'blue',
        code: '#4b71f0'
    },
    {
        _id: 9,
        label: 'Yellow',
        value: 'yellow',
        code: '#fee95f'
    },
    {
        _id: 10,
        label: 'Orange',
        value: 'orange',
        code: '#f5a623'
    },
    {
        _id: 11,
        label: 'White',
        value: 'white',
        code: '#ffffff'
    }
]

let brands = [
    {
        _id: 1,
        name: 'Apple'
    }, {
        _id: 2,
        name: 'Samsung'
    }
]
let models = [
    {
        _id: 1,
        brand: 'Apple',
        name: 'Iphone6'
    }, {
        _id: 2,
        brand: 'Apple',
        name: 'Iphone7'
    }, {
        _id: 3,
        brand: 'Apple',
        name: 'Iphone8'
    }, {
        _id: 4,
        brand: 'Apple',
        name: 'IphoneX'
    }, {
        _id: 5,
        brand: 'Apple',
        name: 'Iphone6s'
    }, {
        _id: 6,
        brand: 'Samsung',
        name: 'S10'
    }, {
        _id: 7,
        brand: 'Samsung',
        name: 'J2'
    }, {
        _id: 8,
        brand: 'Samsung',
        name: 'J5'
    }, {
        _id: 9,
        brand: 'Samsung',
        name: 'J11'
    }]
let themes = [
    {
        _id: 1,
        name: '전부'
    }, {
        _id: 2,
        name: '아트'
    }, {
        _id: 3,
        name: '고양이'
    }, {
        _id: 4,
        name: '클래식'
    }, {
        _id: 5,
        name: '다채로운 색상'
    }, {
        _id: 6,
        name: '이스터'
    }, {
        _id: 7,
        name: '팝아트'
    }, {
        _id: 8,
        name: '음식'
    }, {
        _id: 9,
        name: '기하학적 아트'
    }, {
        _id: 10,
        name: '걸크러쉬'
    }
]
let materials = [
    {
        title: 'Hard Case',
        id: 1,
        price:'12,900',
        name:"하드케이스"
    },
    {
        title: 'Tough Case',
        id: 2,
        price:'15,900',
        name:"터프케이스"
    },
    {
        title: 'Slide Case',
        id: 3,
        price:'17,900',
        name:"슬라이드케이스"
    },
    {
        title: 'Gel Hard Case',
        id: 4,
        price:'12,900',
        name:"젤하드케이스"
    },
    {
        title: 'Door Bumper Case',
        id: 5,
        price:'17,900',
        name:"도어범퍼케이스"
    }
]
let stockData = [{
    _id: 1,
    name: "Test",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'iphone',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'white'
}, {
    _id: 2,
    name: "Test1",
    preview: "https://images-na.ssl-images-amazon.com/images/I/41m6t8sq8qL._SL500_AC_SS350_.jpg",
    count: 1,
    price: 15,
    brand: 'Samsung',
    model: '8',
    material: 'material1',
    color: 'white',
    theme: 'polka'
}, {
    _id: 3,
    name: "Test2",
    preview: "https://cdn-img-1.wanelo.com/p/956/b2d/fb1/4b29e8f1482b297b2088520/x354-q80.jpg",
    count: 1,
    price: 15,
    brand: 'iphone',
    model: '8',
    material: 'material2',
    color: 'black',
    theme: 'rain'
}, {
    _id: 4,
    name: "Test3",
    preview: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRF9NJ-W5CvrXP0Xv7TPfPl29x_1eIK3SdbsZFEKtNqbqgvogHb",
    count: 1,
    price: 15,
    brand: 'Samsung',
    model: '8',
    material: 'material2',
    color: 'black',
    theme: 'white'
}, {
    _id: 5,
    name: "Test4",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'iphone',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'polka'
}, {
    _id: 6,
    name: "Test5",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'Samsung',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'rain'
}, {
    _id: 7,
    name: "Test6",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'Samsung',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'white'
}, {
    _id: 8,
    name: "Test7",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'iphone',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'polka'
}, {
    _id: 9,
    name: "Test8",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'Samsung',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'rain'
}, {
    _id: 10,
    name: "Test9",
    preview: "https://design-milk.com/images/2014/09/polka-rain-iphone6-case.jpg",
    count: 1,
    price: 15,
    brand: 'iphone',
    model: '8',
    material: 'material2',
    color: 'grey',
    theme: 'white'
}]

const getUserCases = (
    _,
    user
) => {
    return new Promise(
        (resolve, reject) => {
            const Case = mongoose.model('Case', CaseSchema);
            const User = mongoose.model('User', UserSchema);
            if (!user.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: user.userId
            },
                (err, existingUser) => {
                    if (existingUser) {
                        Case.find({
                            userId: user.userId
                        },
                            (err, existingCases) => {
                                resolve({
                                    cases: existingCases
                                })
                            })
                    } else {
                        reject(new AuthenticationError(`Please send valid userId`))
                    }
                })
        }
    )
}

const filterStocks = (filters, stocks) => {
    const filteredStocks = stocks.filter(
        stock => {
            let match = true
            Object.keys(filters).map(
                filter => {
                    if (filter != "page" && filters[filter] !== "") {
                        if (!match) {
                            return;
                        }

                        if (filter == "colors") {
                            if (
                                !stock[filter].find(color => {
                                    return color.label == filters[filter]
                                })
                            ) {
                                match = false
                            }
                        } else {
                            if (filter == "theme") {
                                let themes = JSON.parse(filters[filter])
                                if (themes.length > 0) {
                                    if (themes.indexOf(stock[filter]) === -1) {
                                        match = false
                                    }
                                }
                            } else {
                                if (stock[filter] !== filters[filter]) {
                                    match = false
                                }
                            }
                        }
                    }
                }
            )

            return match

        }
    )
    return filteredStocks
}

const getStockCases = (
    _,
    filters
) => {
    return new Promise(
        (resolve, reject) => {
            let StockCases = mongoose.model('StockCases', StockCasesSchema)
            let skip = 10 * (filters.page - 1)
            StockCases
                .find()
                .sort({
                    _id: 'desc'
                })
                .exec((err, existingCases) => {
                    let cases = filterStocks(filters, existingCases)
                    let totalCount = existingCases.length
                    let finalCases = cases.slice(skip, skip + 10)
                    //cases = cases.slice(0, 10)
                    resolve({
                        cases: finalCases,
                        total: totalCount
                    })
                })
        }
    )
}
const getBrands = (
    _,
) => {
    return new Promise(
        (resolve, reject) => {

            resolve({
                list: brands
            })

        }
    )
}
const getMaterials = (
    _,
) => {
    return new Promise(
        (resolve, reject) => {

            resolve({
                list: materials
            })

        }
    )
}
const getModels = (
    _,

) => {
    return new Promise(
        (resolve, reject) => {

            resolve({
                list: models
            })

        }
    )
}
const getColors = (
    _,

) => {
    return new Promise(
        (resolve, reject) => {

            resolve({
                list: colors
            })

        }
    )
}
const getThemes = (
    _,
) => {
    return new Promise(
        (resolve, reject) => {

            resolve({
                list: themes
            })

        }
    )
}

const getUserCasesById = (
    _,
    data
) => {
    return new Promise(
        (resolve, reject) => {
            const Case = mongoose.model('Case', CaseSchema);
            const User = mongoose.model('User', UserSchema);
            if (!data.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: data.userId
            },
                (err, existingUser) => {
                    if (existingUser) {
                        Case.findOne({
                            _id: data.caseId
                        },
                            (err, existingCase) => {
                                if (existingCase) {
                                    resolve(
                                        existingCase
                                    )
                                } else {
                                    reject(new UserInputError(`Please provide valid case id`))
                                }

                            })
                    } else {
                        reject(new AuthenticationError(`Please send valid userId`))
                    }
                })
        }
    )
}

const getStockCasesById = (
    _,
    stock
) => {
    return new Promise(
        (resolve, reject) => {
            const StockCases = mongoose.model('StockCases', StockCasesSchema);
            StockCases.findOne({
                _id: stock.caseId
            },
                (err, existing) => {
                    if (existing) {
                        resolve(existing)
                    } else {
                        reject(new AuthenticationError(`Please send valid userId`))
                    }
                })
        }
    )
}

const deleteStockCase = (
    _,
    caseData
) => {

    return new Promise(
        (resolve, reject) => {

            const StockCases = mongoose.model('StockCases', StockCasesSchema);
            const User = mongoose.model('User', UserSchema);
            if (!caseData.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: caseData.userId
            },
                (err, existingUser) => {
                    if (existingUser) {
                        if (existingUser.role === "Admin")
                            StockCases.deleteOne({ "_id": caseData.caseId }).then((res) => {
                                resolve({
                                    message: "재고가 삭제되었습니다"
                                })
                            })
                    } else {
                        reject(new AuthenticationError(`Please send valid userId`))
                    }
                })

        }
    )
}

const getCaseModel = (
    _,
) => {
    return new Promise(
        (resolve, reject) => {
            const Model = mongoose.model('model', ModelSchema)
            Model.find({
            },
                (err, existingModel) => {
                    if (existingModel) {
                        resolve({
                            list: existingModel
                        })
                    } else {
                        reject(new UserInputError(`잠시후 다시 시도해주세요`))
                    }
                })
        }
    )
}


module.exports = {
    getUserCases,
    getUserCasesById,
    getStockCases,
    getBrands,
    getMaterials,
    getModels,
    getColors,
    getThemes,
    getStockCasesById,
    deleteStockCase,
    getCaseModel
}