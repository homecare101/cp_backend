const { AuthenticationError } = require('apollo-server');

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const ModelArr = require('../../json/cases.js')
const { UserSchema, AddressSchema, ModelSchema} = require('../../schema/index')

const {
    addTokenToUser
} = require('../../utils/authentication')

const socialLogin = (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('user', UserSchema)
            if (user.provider) {
                const { provider, providerId, email } = user
                User.findOne(
                    { 
                        provider,
                        providerId,
                        email
                    },
                    (err, doc) => {
                        if (doc) {
                            resolve(
                                addTokenToUser(doc)
                            )
                        }
                        else {
                            const newUser = new User({
                                ...user,
                                role: "User"
                            })
                            newUser.save().then(
                                (response, err) => {
                                    resolve(addTokenToUser(response))
                                }
                            )
                        }
                    }
                )
            } else {
                reject(new AuthenticationError(`Invalid Request`))
            }
        }
    )
}
const getAllUsers = (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('user', UserSchema)
                User.findOne(
                    { 
                        _id: user.userId
                    },
                    (err, user) => {
                        if (user && user.role === "Admin") {
                            User.find({
                            },
                            (err, existingOrders) => {
                                resolve ({usersList : existingOrders})
                            })   
                        }
                        else {
                            reject(new AuthenticationError(`해당 권한이 없습니다`))
                        }
                    }
                )
        }
    )
}
const login = (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('user', UserSchema)
            if (user.provider) {
                const { provider, providerId } = user
                User.findOne(
                    { 
                        provider,
                        providerId
                    },
                    (err, doc) => {
                        if (doc) {
                            resolve(
                                addTokenToUser(doc)
                            )
                        }
                        else {
                            reject(new AuthenticationError(`계정이 존재하지 않습니다`))
                        }
                    }
                )
            } else {
                const { email, password } = user
                User.findOne(
                    { email: email },
                    (err, doc) => {
                        if (doc) {
                            if (bcrypt.compareSync(password, doc.password)) {
                                resolve(
                                    addTokenToUser(doc)
                                )
                            } 
                            else {
                                reject(new AuthenticationError(`잘못된 비밀번호 또는 전자 메일을 입력했습니다`))
                            }
                        } else {
                            reject(new AuthenticationError(`계정이 존재하지 않습니다`))
                        }
                    }
                )
            }
        }
    )
}

const getAddress =  (
    _,
    userId
) => {
    return new Promise (
        (resolve, reject) => {
            const Address = mongoose.model('Address', AddressSchema);
            const User = mongoose.model('User', UserSchema);
            if (!userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: userId.userId
            },
            (err, existingUser) => {
                if(existingUser){
                    Address.find({userId: userId.userId},(error, response)=>{
                        resolve({
                            addresses: response
                        })
                    })
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
        }
    )


}

const deleteAddress =  (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('User', UserSchema);
            const Address = mongoose.model('Address', AddressSchema);
            if (!user.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: user.userId
            },
            (err, existingUser) => {
                if(existingUser){
                    Address.deleteOne( { "_id" : user.addressId} ).then((res)=>{
                        resolve({
                            message: "주소가 삭제되었습니다"
                        })
                    })
                } else {
                     reject(new AuthenticationError(`다시 로그인 해주세요`))
                }
            })
        }
    )
}


const checkUser =  (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('User', UserSchema);
            if (!user.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: user.userId,
                token: user.token
            },
            (err, existingUser) => {
                if(existingUser){
                    resolve({
                        message: "Authentication true"
                    })
                } else {
                     reject(new AuthenticationError(`다시 로그인 해주세요`))
                }
            })
        }
    )
}

const getUserById =  (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('User', UserSchema);
            if (!user.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: user.userId,
            },
            (err, existingUser) => {
                if(existingUser){
                    resolve(existingUser)
                } else {
                     reject(new AuthenticationError(`다시 로그인 해주세요`))
                }
            })
        }
    )
}

const sendModels =  (
    _,
) => {
    return new Promise (
        (resolve, reject) => {
            const Model = mongoose.model('model', ModelSchema);
            Model.insertMany(ModelArr).then(results => {
                resolve({
                    message: "Done"
                })
              });
        }
    )
}
module.exports = {
    login,
    socialLogin,
    getAddress,
    deleteAddress,
    checkUser,
    getAllUsers,
    getUserById,
    sendModels
}