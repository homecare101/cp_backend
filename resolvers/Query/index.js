const UserQueries = require('./User');
const CaseQueries = require('./Case')
const PaymentQueries = require('./Payment')
const CartQueries = require('./Cart')
module.exports = {
    ...UserQueries,
    ...PaymentQueries,
    ...CaseQueries,
    ...CartQueries
}