const { AuthenticationError, UserInputError } = require('apollo-server');

const mongoose = require('mongoose');

const { UserSchema, PaymentSchema} = require('../../schema/index')

 const deletePaymentMethod = (
    _,
    payment
	) => {

    return new Promise (
        (resolve, reject) => {

        const Payment = mongoose.model('Payment', PaymentSchema);
            const User = mongoose.model('User', UserSchema);
            if (!payment.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: payment.userId
            },
            (err, existingUser) => {
                if(existingUser){
                   Payment.findOne({
		                _id: payment.paymentId
		            },
		            (err, existingPayment) => {
		                if(existingPayment){ 
		                	Payment.deleteOne( { "_id" : existingPayment._id } ).then((res)=>{
		                		resolve({
		                			message: "결제정보가 삭제되었습니다"
		                		})
		                	})
		                } else {
		                     reject(new UserInputError(`유효한 결제 아이디를 보내주세요`))
		                }
		            })     
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
             })

 		}
 	)
 }

 const getPaymentMethod = (
    _,
    payment
	) => {

    return new Promise (
        (resolve, reject) => {
        const Payment = mongoose.model('Payment', PaymentSchema);
        const User = mongoose.model('User', UserSchema);
        if (!payment.userId) {
            reject(new UserInputError(`사용자 아이디를 입력해주세요`))
            return
        }
        User.findOne({
            _id: payment.userId
        },
        (err, existingUser) => {
            if(existingUser){
               Payment.find({
	                userId: payment.userId
	            },
	            (err, existingPayment) => {
	                	resolve ({
	                		paymentMethod : existingPayment
	                	})
	            })     
            } else {
                 reject(new AuthenticationError(`Please send valid userId`))
            }
         })

 		}
    )
}

const selectPrimaryCard = (
    _,
    card
    ) => {

    return new Promise (
        (resolve, reject) => {
        const Payment = mongoose.model('Payment', PaymentSchema);
        const User = mongoose.model('User', UserSchema);
        if (!card.userId) {
            reject(new UserInputError(`사용자 아이디를 입력해주세요`))
            return
        }
        User.findOne({
            _id: card.userId
        },
        (err, existingUser) => {
            if(existingUser){
               Payment.findOne({
                    _id: card.cardId
                },
                (err, existingPayment) => {
                    if(existingPayment){
                        Payment.findOneAndUpdate(
                            { 
                                _id : card.cardId },
                            {
                                $set: {
                                    selected: true
                                }
                            },
                            () => {
                                Payment.findOneAndUpdate(
                                    { _id : { $ne: card.cardId },
                                        userId : card.userId,
                                        selected : true },
                                    {
                                        $set: {
                                            selected: false
                                        }
                                    },
                                    () => {
                                        
                                    }
                                )
                                resolve({
                                    message: '주결제 정보가 변경되었습니다'
                                })
                            }
                        )
                    } else {
                        reject(new UserInputError(`유효한 결제 아이디를 보내주세요`))
                    }
                })     
            } else {
                 reject(new AuthenticationError(`Please send valid userId`))
            }
         })

        }
    )
}

module.exports = {
    deletePaymentMethod,
    getPaymentMethod,
    selectPrimaryCard
}