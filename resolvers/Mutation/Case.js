const { CaseSchema, UserSchema, OrderSchema} = require('../../schema/index')
const { AuthenticationError, UserInputError } = require('apollo-server');
const gcd = (a, b) => {
    return (b == 0) ? a : gcd (b, a%b);
}

const gm = require('gm').subClass({imageMagick: true});
const mongoose = require('mongoose');
const request = require('request');
const phantom = require('phantom');
const uuidV4 = require('uuid/v4');
const path = require('path');
const fs = require('fs');
const generateName = () => (`${uuidV4()}.png`)
const assets_dir = path.join(__dirname, '../assets/images');
const saveCase = (
    _,
    saveCase
) => {
    return new Promise (
        (resolve, reject) => {
        const Case = mongoose.model('Case', CaseSchema);
        const User = mongoose.model('User', UserSchema);
        User.findOne({
            _id: saveCase.userId
            },
            (err, existingUser) => {
                 if(existingUser){
                    getPreviewImage(saveCase.preview,"savePreview").then(result => {
                        const newCase = new Case({
                            ...saveCase,
                            preview: result.newPreview
                        })
                        newCase.save().then(
                            (response, err) => {
                                fs.unlink(assets_dir+ '/' + result.name)
                                resolve(
                                    response
                                )
                            }
                        )
                    })
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
        }
    )
}

const downloadImage = (filename) => {
    request.head(assets_dir+ '/' + filename, function(err, res, body){
        request(assets_dir+ '/' + filename).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

const getPreviewImage = (preview,type) => {
    return new Promise (
        (resolve, reject) => {
            let fileName = new Date().valueOf()+".html"
            let writeStream = fs.createWriteStream(fileName);
            writeStream.write("<html> <head></head> <body>" + preview + "</body></html>");
            writeStream.end();
            let pathName = path.join(__dirname, '../../'+ fileName);
            let imagetype = type
            let inches = {
                width: 4,
                height: 7
            }
            const dpi = 300

            let ratio = {
                width: 1360 / 284.5,
                height: 768 / 455
            }

            let printDimensions = {
                width: ratio.width * (inches.width * dpi),
                height: ratio.height * (inches.height * dpi)
            }
            takePhoto(`file:///${pathName}`, printDimensions, fileName,imagetype).then(name => {
                if(imagetype === "savePreview"){
                    let bitmap = fs.readFileSync(assets_dir+ '/' + name);
                    let newPreview = new Buffer(bitmap).toString('base64');
                    newPreview = 'data:image/png;base64,' + newPreview
                    return resolve({newPreview: newPreview, name: name})
                } else {
                    const newFileName = "final_" + name
                    gm(assets_dir+ '/' + name)
                    .colorspace('CMYK')
                    .write(assets_dir+ '/' + newFileName , function (err) {
                        if (err) console.log(err,'err') 
                        else{
                            fs.unlink(assets_dir+ '/' + name);
                            let bitmap = fs.readFileSync(assets_dir+ '/' + newFileName);
                            let newPreview = new Buffer(bitmap).toString('base64');
                            newPreview = 'data:image/jpeg;base64,' + newPreview
                            return resolve({newPreview: newPreview, name: newFileName})
                        }
                    });
                }
            }).catch((error)=>{
                console.log(error)
            })
        })
}


const editCase = (
    _,
    data
) => {
    return new Promise (
        (resolve, reject) => {
            console.log(data,"datadatadatadatadatadata")
        const Case = mongoose.model('Case', CaseSchema);
        const User = mongoose.model('User', UserSchema);
            User.findOne({
            _id: data.userId
            },
            (err, existingUser) => {
                if(existingUser){
                    Case.findOne({
                                _id: data.caseId
                                },
                                (err, doc) => {
                                    if(doc){
                                        
                                        getPreviewImage(data.preview,"savePreview").then(result => {
                                            Case.findByIdAndUpdate(
                                                doc._id,
                                                {
                                                    $set: {
                                                        ...data,
                                                        preview: result.newPreview
                                                    }
                                                },
                                                () => {
                                                    fs.unlink(assets_dir+ '/' + result.name)
                                                    resolve({
                                                        message: "케이스가 수정되었습니다"
                                                    })
                                                }
                                            )
                                        })
                                    }else{
                                        reject(new UserInputError(`Please send valid caseId`))
                                    }
                                    
                                })
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
        }
    )
}

const takePhoto = (url, viewport, createdFile,type) => {
	return new Promise ((res, rej) => {
		let phInstance = null;
       
		phantom.create(['--ignore-ssl-errors=yes'])
			.then((instance) => {
				phInstance = instance;
				return instance.createPage();
			})
			.then((page) =>{ 
                
				page.property('viewportSize', viewport).then(()=>{
					page.open(url)
						.then(async (status)=>{
							if(status != 'fail') {
                                let filename = generateName();
                                let path = assets_dir + '/' + filename;
                                var clipRect = await page.evaluate(function(){
                                    const reqElem = document.getElementById("phoneModelPath")
                                    return reqElem.getBoundingClientRect()
                                });
                                // if(type == "download"){
                                //     var clipRect = await page.evaluate(function(){
                                //         const pathPart = document.getElementById('printableHidden').getAttribute("d")
                                //         const childElement = document.getElementById('phoneModelPath')
                                //         childElement.removeAttribute("d")
                                //         childElement.setAttribute("d", "pathPart")
                                //         const reqElem = document.getElementsByClassName('respectiveClip');
                                //         return reqElem[0].getBoundingClientRect()
                                //     });
                                // } else {
                                //     var clipRect = await page.evaluate(function(){
                                //         const reqElem = document.getElementsByClassName('respectiveClip');
                                //         return reqElem[0].getBoundingClientRect()
                                //     });
                                // }
                                page.property('clipRect', clipRect)
                                setTimeout(function () {
                                    page.render(path)
                                        .then(function () {
                                            phInstance.exit();
                                            fs.unlink(createdFile);
                                            return res(filename);
                                        })
                                }, 200);
							} else {
								return rej('Failed! Make sure you entered a valid url');
								phInstance.exit();
							}
						})
				});
			})
			.catch(function (error) {
				return rej(error);
				phInstance.exit();
			});
	})
}

const downloadOrderCases = (
    _,
    data
) => {
    return new Promise (
        (resolve, reject) => {
        const Order = mongoose.model('Order', OrderSchema);
        const User = mongoose.model('User', UserSchema);
            User.findOne({
                _id: data.userId
            },
            (err, existingUser) => {
                if(existingUser.role === "Admin"){
                    Order.findOne({
                        _id: data.orderId
                        },
                        (err, order) => {
                            if(order){
                                let imageArr = [], count = 1
                                order.items.forEach( async (element) => {
                                    const image = await getPreviewImage(element.outlineSvgData,"download").then(result => {
                                       return result
                                    })
                                    imageArr.push({path : image.newPreview, fileName: image.name})
                                    if(count === order.items.length){
                                        resolve({imageArr: imageArr, message: "이미지가 다운로드 중입니다"})
                                        imageArr.forEach(image => {
                                            fs.unlink(assets_dir+ '/' + image.fileName)
                                        })
                                    }
                                    count = count + 1
                                });
                                
                            }else{
                                reject(new UserInputError(`유효한 caseId를 보내주십시오`))
                            }
                            
                        })
                } else {
                     reject(new AuthenticationError(`관리자만 다운로드가 가능합니다`))
                }
            })
        }
    )
}

module.exports = {
    saveCase,
    editCase,
    downloadOrderCases
}
