const { StockCasesSchema, UserSchema} = require('../../schema/index')
const { AuthenticationError, UserInputError } = require('apollo-server');

const mongoose = require('mongoose');

const addStockCase = (
    _,
    caseData
) => {
    return new Promise (
        (resolve, reject) => {
        const StockCases = mongoose.model('StockCases', StockCasesSchema);
        const User = mongoose.model('User', UserSchema);
        User.findOne({
            _id: caseData.userId
        },
        (err, existingUser) => {
            if(existingUser){
                if(existingUser.role === "Admin"){
                    const newStockCase = new StockCases({
                        ...caseData
                    })
                    newStockCase.save().then(
                        (response, err) => {
                            resolve({
                             message: "새로운 케이스가 상점에 추가되었습니다"
                            })
                    })
                 } else {
                    reject(new AuthenticationError(`Please send valid userId`))
                 }
                }
                
            })
        }
    )
}


const editStockCase = (
    _,
    caseData
) => {
    return new Promise (
        (resolve, reject) => {
        const StockCases = mongoose.model('StockCases', StockCasesSchema);
        const User = mongoose.model('User', UserSchema);
        User.findOne({
            _id: caseData.userId
        },
        (err, existingUser) => {
            if(existingUser){
                if(existingUser.role === "Admin"){
                    StockCases.findByIdAndUpdate(
                        caseData.caseId,
                        {
                            $set: {
                                price: caseData.price,
                                name: caseData.name,
                                preview: caseData.preview,
                                brand: caseData.brand,
                                model: caseData.model,
                                material: caseData.material,
                                color: caseData.color,
                                theme: caseData.theme
                            }
                        },
                        () => {
                            resolve({
                                message: "제품이 업데이트 되었습니다"
                            })
                        }
                    )
                 } else {
                    reject(new AuthenticationError(`Please send valid userId`))
                 }
                }
                
            })
        }
    )
}


module.exports = {
    addStockCase,
    editStockCase
}