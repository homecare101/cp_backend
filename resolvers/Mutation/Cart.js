const { CartSchema, UserSchema} = require('../../schema/index')
const { AuthenticationError, UserInputError } = require('apollo-server');

const mongoose = require('mongoose');

const addToCart = (
    _,
    cartData
) => {
    return new Promise (
        (resolve, reject) => {
        const User = mongoose.model('User', UserSchema);
        const Cart = mongoose.model('Cart', CartSchema);
	            User.findOne({
	            _id: cartData.userId
	            },
	            (err, existingUser) => {
	                if(existingUser){
                	let cartItem = cartData.items, subtotal= 0;
                	cartItem.forEach( item => {
                		subtotal = subtotal + item.price
                    })
                	Cart.findOneAndUpdate(
                            { _id: cartData.cartId },
                            {
                                $set: {
                                    items: cartItem,
                                    subtotal: subtotal
                                }
                            },
                            () => {
                                resolve({
                                    message: '장바구니에 담겼습니다'
                                })
                            }
                        )
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
        }
    )
}

const checkCart = (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
        const User = mongoose.model('User', UserSchema);
        const Cart = mongoose.model('Cart', CartSchema);
            User.findOne({
            _id: user.userId
            },
            (err, existingUser) => {
                if(existingUser){
                	Cart.findOne({
			            userId: user.userId
			            },
			            (err, existingCart) => {
			            	if(existingCart){
			            		resolve(existingCart)
			            	} else {
			            		const newCart = new Cart({
								    subTotal: 0,
								    items: [],
								    userId: user.userId,
			                    })
			                    newCart.save().then(
			                        (response, err) => {
			                            resolve(
			                               response
			                            )
			                        }
			                    )
			            	}
			            })
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
        }
    )
}


module.exports = {
	addToCart,
	checkCart
}