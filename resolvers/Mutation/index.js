const UserMutations = require('./User');
const ContactUsMutations = require('./ContactUs');
const CaseMutations = require('./Case');
const PaymentMutations = require('./Payment')
const CartMutations = require('./Cart')
const OrderMutations = require('./Order')
const StockMutations = require('./Stock')
module.exports = {
    ...UserMutations,
    ...ContactUsMutations,
    ...CaseMutations,
    ...PaymentMutations,
    ...CartMutations,
    ...StockMutations,
    ...OrderMutations
}