const { AuthenticationError, UserInputError } = require('apollo-server');

const uuidv4 = require('uuid/v4')
const nodemailer = require('nodemailer');

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { UserSchema, AddressSchema} = require('../../schema/index')

var transporter = nodemailer.createTransport({
    service: 'gmail',
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
           user: 'customparkdemo@gmail.com',
           pass: 'Heena_0987'
       }
});

const {
    addTokenToUser
} = require('../../utils/authentication')

const forgotPassword = (
    _,
    user
) => {

    const User = mongoose.model('User', UserSchema);

    return new Promise (
       (resolve, reject) => {

            User.findOne(
                { email: user.email },
                (err, doc) => {
        
                    if (doc) {

                        verificationCode = uuidv4();

                        User.findByIdAndUpdate(
                            doc._id,
                            {
                                verificationCode: verificationCode,
                            },
                            { new: true },
                            (err, document) => {

                                const mailOptions = {
                                    from: 'customparkdemo@gmail.com', // sender address
                                    to: user.email, // list of receivers
                                    subject: 'Subject of your email', // Subject line
                                    html: `
                                        <p>Click on the link to reset <a target="_blank" href="https://custompark.co.kr?verificationCode=${verificationCode}">
                                            https://custompark.co.kr?verificationCode=${verificationCode}
                                        </a></p>
                                    `
                                };
                
                                transporter.sendMail(mailOptions, function (mailError) {
                                    if (mailError) {

                                        reject(new AuthenticationError(`서버 에러가 발생했습니다`))

                                    } else {

                                        resolve({
                                            message: '이메일이 발송되었습니다 이메일을 확인해주세요'
                                        })

                                    }
                                });

                            }
                        )
        
                    } else {
        
                        reject(new AuthenticationError(`해당 아이디는 계정이 없습니다`))
        
                    }
        
                }
            )

       }
   )

}

const resetPassword =  (
    _,
    user
) => {

    return new Promise (
        (resolve, reject) => {

            const User = mongoose.model('User', UserSchema);

            if (!user.verificationCode) {
                reject(new UserInputError(`인증코드를 입력하세요`))
                return
            }

            if (!user.password) {
                reject(new UserInputError(`비밀번호를 입력하세요`))
                return
            }

            User.findOne(
                { verificationCode: user.verificationCode },
                ( err, doc ) => {

                    if (doc) {

                        var salt = bcrypt.genSaltSync(10);

                        var passwordToSave = bcrypt.hashSync(user.password, salt)

                        User.findByIdAndUpdate(
                            doc._id,
                            {
                                $set: {
                                    verificationCode: '',
                                    password: passwordToSave
                                }
                            },
                            () => {
                                resolve({
                                    message: '비밀번호가 변경되었습니다'
                                })
                            }
                        )

                    } else {

                        reject(new AuthenticationError(`계정이 존재하지 않습니다`))

                    }

                }
            )

        }
    )


}


const signup = (
    _,
    user
) => {

    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('User', UserSchema);
            User.findOne({
                email: user.email
            },
            (err, existingUser) => {
                if (existingUser) {
                    reject(new AuthenticationError('이미 계정이 존재합니다'))
                } else {
                    var salt = bcrypt.genSaltSync(10);
                    var passwordToSave = bcrypt.hashSync(user.password, salt)
                    const newUser = new User({
                        ...user,
                        password: passwordToSave,
                        role: "User"
                    })
                    newUser.save().then(
                        (response, err) => {
                            resolve(
                                addTokenToUser(response)
                            )
                        }
                    )
                }
            })
        }
    )
}

const addAddress =  (
    _,
    address
) => {
    return new Promise (
        (resolve, reject) => {
            const Address = mongoose.model('Address', AddressSchema);
            const User = mongoose.model('User', UserSchema);
            if (!address.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: address.userId
            },
            (err, existingUser) => {
                if(existingUser){
                    const newAddress = new Address({
                        ...address,
                    })
                    newAddress.save().then(
                        (response, err) => {
                            resolve(
                                response
                            )
                        }
                    )
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
            
        }
    )


}

const editAddress =  (
    _,
    address
) => {
    return new Promise (
        (resolve, reject) => {
            const Address = mongoose.model('Address', AddressSchema);
            const User = mongoose.model('User', UserSchema);
            if (!address.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: address.userId
            },
            (err, existingUser) => {
                if(existingUser){
                            Address.findOne({
                                _id: address.addressId
                                },
                                (err, doc) => {
                                    if(doc){
                                        Address.findByIdAndUpdate(
                                            doc._id,
                                            {
                                                $set: {
                                                    extraAddress: address.extraAddress,
                                                    fullAddress: address.fullAddress,
                                                    zonecode: address.zonecode,
                                                }
                                            },
                                            () => {
                                                resolve({
                                                    message: "주소가 변경되었습니다"
                                                })
                                            }
                                        )
                                    }else{
                                        reject(new AuthenticationError(`유효한 주소아이디가 필요합니다`))
                                    }
                                    
                                })
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
             })
        }
    )
}

const editUser =  (
    _,
    user
) => {
    return new Promise (
        (resolve, reject) => {
            const User = mongoose.model('User', UserSchema);
            if (!user.userId) {
                reject(new UserInputError(`사용자 아이디를 입력해주세요`))
                return
            }
            User.findOne({
                _id: user.userId
            },
            (err, existingUser) => {
                if(existingUser){
                    User.findOne({
                        _id: {$ne: user.userId},
                        email: user.email
                    },
                    (err, existingUserWithEmail) => {
                        if(existingUserWithEmail){
                            reject(new UserInputError(`해당 이메일로 아이디가 존재합니다`))
                        } else {
                            User.findByIdAndUpdate(
                                existingUser._id,
                                {
                                    $set: {
                                        email: user.email,
                                        lastName: user.lastName,
                                        firstName: user.firstName,
                                        userName: user.userName,
                                        gender: user.gender,
                                        phone: user.phone, 
                                    }
                                },
                                (data) => {
                                    resolve({
                                        message: "사용자 정보가 수정되었습니다"
                                    })
                                }
                            )
                        }
                    })
                    
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
             })
            
        }
    )
}


const changeUserPassword =  (
    _,
    user
) => {

    return new Promise (
        (resolve, reject) => {

            const User = mongoose.model('User', UserSchema);

            if (!user.userId) {
                reject(new UserInputError(`Please provide a userId`))
                return
            }

            if (!user.password) {
                reject(new UserInputError(`비밀번호를 입력하세요`))
                return
            }

            User.findOne(
                { _id: user.userId },
                ( err, doc ) => {
                    if (doc) {
                        let salt = bcrypt.genSaltSync(10);
                        let newPasswordToSave = bcrypt.hashSync(user.newPassword, salt)
                        if(bcrypt.compareSync(user.password, doc.password)){
                            User.findByIdAndUpdate(
                                doc._id,
                                {
                                    $set: {
                                        password: newPasswordToSave
                                    }
                                },
                                () => {
                                    resolve({
                                        message: '제품이 업데이트 되었습니다'
                                    })
                                }
                            )
                        } else {
                            reject(new UserInputError(`이전 비밀번호가 변경되었습니다`))
                        }
                    } else {

                        reject(new AuthenticationError(`해당 아이디는 계정이 없습니다`))

                    }

                }
            )

        }
    )


}

module.exports = {
    signup,
    forgotPassword,
    resetPassword,
    addAddress,
    editAddress,
    editUser,
    changeUserPassword
}