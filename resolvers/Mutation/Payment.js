const { AuthenticationError, UserInputError } = require('apollo-server');
const { PaymentSchema, UserSchema} = require('../../schema/index')
const mongoose = require('mongoose');

const addPaymentMethod = (
    _,
    payment
) => {

    return new Promise (
        (resolve, reject) => {

        const Payment = mongoose.model('Payment', PaymentSchema);
            const User = mongoose.model('User', UserSchema);
            if (!payment.userId) {
                reject(new UserInputError(`Please provide user id`))
                return
            }
            User.findOne({
                _id: payment.userId
            },
            (err, existingUser) => {
                if(existingUser){
                   const newPayment = new Payment({
                        ...payment,
                    })
                    newPayment.save().then(
                        (response, err) => {
                            resolve(
                                response
                            )
                        }
                    )         
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
             })

 		}
 	)
 }

module.exports = {
    addPaymentMethod
}
