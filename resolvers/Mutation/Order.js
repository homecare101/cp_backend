const { OrderSchema, UserSchema, CartSchema} = require('../../schema/index')
const { AuthenticationError, UserInputError } = require('apollo-server');

const mongoose = require('mongoose');

const saveOrder = (
    _,
    order
) => {
    return new Promise (
        (resolve, reject) => {
        const Order = mongoose.model('Order', OrderSchema);
        const User = mongoose.model('User', UserSchema);
        const Cart = mongoose.model('Cart', CartSchema);
            User.findOne({
            _id: order.userId
            },
            (err, existingUser) => {
                if(existingUser){
                    const newOrder = new Order({
                        ...order
                    })
                    newOrder.save().then(
                        (response, err) => {
                            Cart.findOneAndUpdate(
                                { _id: order.cartId },
                                {
                                    $set: {
                                        items: [],
                                        subtotal: 0
                                    }
                                },
                                () => {
                                    resolve({
                                        message: '주문이 완료되었습니다'
                                    })
                                }
                            )
                        }
                    )
                } else {
                     reject(new AuthenticationError(`Please send valid userId`))
                }
            })
        }
    )
}

module.exports = {
    saveOrder
}
