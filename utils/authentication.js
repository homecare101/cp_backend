const jwt = require('jsonwebtoken');

const constants = require('../constants')

const generateToken = payload => jwt.sign(convertToPlainObject(payload), constants.superSecret);

const convertToPlainObject = payload => JSON.parse(
    JSON.stringify(payload)
)

addTokenToUser = user => {

    let plainUser = convertToPlainObject(user)
    
    const token = generateToken(user)

    plainUser = {
        ...plainUser,
        token
    }

    return plainUser

}

module.exports = {
    generateToken,
    addTokenToUser,
    convertToPlainObject
}