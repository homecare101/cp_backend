const { gql } = require('apollo-server-express');

const Mutations = gql`
    type Mutation {
        addPaymentMethod (
            userId: String,
			accNo: String,
			date: String,
			selected: Boolean
        ) : Payment
    }
`

module.exports = Mutations