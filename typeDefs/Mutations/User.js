const { gql } = require('apollo-server-express');

const Mutations = gql`
    type Mutation {
        signup (
            email: String,
            password: String,
            lastName: String,
            firstName: String
        ): User,
        
        editUser (
            userId: String,
            email: String,
            lastName: String,
            firstName: String,
            userName: String,
            gender: String,
            phone: String,
        ): User,

        forgotPassword (
            email: String
        ): Message

        resetPassword (
            verificationCode: String,
            password: String
        ): Message

        changeUserPassword (
            userId: String,
            password: String,
            newPassword: String
        ): Message

        contactUs (
            name: String,
            email: String,
            message: String
        ): Message

        addAddress (
            userId: String,
            extraAddress: String,
            fullAddress: String,
            zonecode: String
        ) : Address

        editAddress(
            addressId: String,
            userId: String,
            extraAddress: String,
            fullAddress: String,
            zonecode: String,
        ) : Message
        
    }
`

module.exports = Mutations