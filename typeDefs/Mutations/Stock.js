const { gql } = require('apollo-server-express');

const Mutations = gql`
    type Mutation {
        addStockCase (
            userId: String,
            count: Int,
            price: Int,
            name: String,
            preview: String,
            brand: String,
            model: String,
            material: String,
            productDetail: String,
            colors: [colorToSend!]!,
            theme: String
        ) : Message
        editStockCase (
            userId: String,
            caseId: String,
            count: Int,
            price: Int,
            name: String,
            preview: String,
            brand: String,
            model: String,
            material: String,
            productDetail: String,
            colors: [colorToSend!]!,
            theme: String
        ) : Message
    }
`

module.exports = Mutations