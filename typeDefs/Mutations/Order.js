const { gql } = require('apollo-server-express');

const Mutations = gql`
    type Mutation {
        saveOrder (
            status: String,
            userId: String,
            firstName: String,
            lastName: String,
            extraAddress: String,
            fullAddress: String,
            zonecode: String,
            extraDetails: String,
            mobileNumber: String,
            email: String,
            orderNotes: String,
            createdAt: String,
            cartId: String,
            subTotal: String,
            items: [CaseToSend!]!,
        ) : Message

        downloadOrderCases(
            userId : String,
            orderId : String,
        ) : ImageArr
    }
`

module.exports = Mutations