const mergeGraphqlSchemas = require('merge-graphql-schemas');

const UserMutations = require('./User');

const EditorMutations = require('./Editor');

const PaymentMutations = require('./Payment');

const CartMutations = require('./Cart');

const StockMutations = require('./Stock');

const OrderMutations = require('./Order');

module.exports = mergeGraphqlSchemas.mergeTypes([UserMutations, EditorMutations, PaymentMutations, CartMutations,StockMutations, OrderMutations], { all: true })