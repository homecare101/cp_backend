const { gql } = require('apollo-server-express');

const Mutations = gql`
    type Mutation {
        addToCart (
            subTotal: Int,
		    items: [CaseToSend!]!,
		    userId: String,
            cartId: String
        ) : Message
        checkCart (
		    userId: String,
        ) : Cart
    }
`

module.exports = Mutations