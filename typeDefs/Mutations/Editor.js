const { gql } = require('apollo-server-express');

const Mutations = gql`
    type Mutation {
        saveCase (
            caseData: String,
            name: String,
            count: Int,
            price: Int,
            userId: String,
            preview: String,
            brand: String,
            model: String,
            material: String,
            outlineSvgData: String
        ) : Case

        editCase (
        	caseId: String,
        	caseData: String,
            name: String,
            price: Int,
            userId: String,
            preview: String,
            brand: String,
            model: String,
            material: String,
            outlineSvgData: String
        ) : Message
    }
`

module.exports = Mutations