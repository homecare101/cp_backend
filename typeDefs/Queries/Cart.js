const { gql } = require('apollo-server-express');

const Query = gql`
    type Query {
        getOrderData (
            userId: String
        ): UserCart
        getAllOrders (
            userId: String
        ): UserCart
        changeOrderStatus (
            userId: String,
            orderId: String,
            status: String
        ): Message
        getCartData (
            userId: String
        ): Cart
    }
`

module.exports = Query