const mergeGraphqlSchemas = require('merge-graphql-schemas');

const UserQueries = require('./User');

const PaymentQueries = require('./Payment');

const CaseQueries = require('./Case');

const CartQueries = require('./Cart');

module.exports = mergeGraphqlSchemas.mergeTypes([UserQueries, PaymentQueries, CaseQueries, CartQueries], { all: true })