const { gql } = require('apollo-server-express');

const Query = gql`
    type Query {
        login (
            email: String,
            password: String,
        ): User

        getUserById(
            userId: String
        ): User

        socialLogin (
            email: String,
            firstName: String,
            lastName: String,
            provider: String,
            providerId: String
        ): User

        getAddress (
            userId: String
        ) : UserAddress

        deleteAddress (
            userId: String,
            addressId: String
        ) : Message

        checkUser (
            userId: String,
            token: String
        ) : Message

        getAllUsers (
            userId : String
        ) : Users

        sendModels : Message
    }
`

module.exports = Query