const { gql } = require('apollo-server-express');

const Query = gql`
    type Query {
        deletePaymentMethod(
	        paymentId: String,
	        userId: String,
		): Message
		getPaymentMethod(
	        userId: String,
		): UserPayment
		selectPrimaryCard(
			userId: String,
			cardId: String
		): Message
    }
`

module.exports = Query

