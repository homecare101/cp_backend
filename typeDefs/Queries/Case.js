const { gql } = require('apollo-server-express');

const Query = gql`
    type Query {
        getUserCases (
            userId: String
        ): UserCases
        
        getUserCasesById (
        	userId: String,
        	caseId: String
        ): Case

        getStockCasesById (
        	caseId: String
        ): Case

        getStockCases (
            brand: String,
            model: String,
            material: String,
            colors: String,
            theme: String,
            page: Int
        ): StockCases

        deleteStockCase(
            userId: String
            caseId: String
        ): Message

        getCaseModel: Models

        getBrands: DataWithNameList
        getMaterials: DataWithNameList
        getModels: DataWithNameList
        getColors: colorList
        getThemes: DataWithNameList
    }
`

module.exports = Query