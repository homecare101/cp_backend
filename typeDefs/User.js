const { gql } = require('apollo-server-express');

module.exports = gql`
    type Users {
        usersList : [User]
    }
    type User {
        _id: String,
        email: String,
        firstName: String,
        lastName: String,
        provider: String,
        address: String,
        userName: String,
        gender: String,
        phone: String,
        providerId: String,
        role: String,
        token: String
    }

`