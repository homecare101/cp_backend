const { gql } = require('apollo-server-express');

module.exports = gql`

    type DataWithName {
        _id: Int,
        name: String,
        brand: String,
        title:String,
        price:String,
        id:Int
    }
    type DataWithNameList {
        list: [DataWithName]
    }

    type StockCase {
        _id: String,
        name: String,
        preview: String,
        count: Int,
        price: Int,
        brand: String,
        model: String,
        material: String,
        colors:[color],
        theme:String
    }

    type StockCases {
        cases: [StockCase],
        total: String
    }

`
