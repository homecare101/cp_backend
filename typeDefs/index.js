const mergeGraphqlSchemas = require('merge-graphql-schemas');
const { gql } = require('apollo-server-express');

const Mutations = require('./Mutations')
const Queries = require('./Queries')

const UserType = require('./User');
const caseType = require('./Case');
const typeDefs = gql`

# type can be used in other type declarations.

  type Message {
    message: String
  }

  type ImageArr {
    message: String,
    imageArr : [path]
  }

  type path {
    path: String
  }

  type UserAddress{
    addresses:[Address]
  }

  type UserPayment{
    paymentMethod: [Payment]
  }

  type UserCases{
    cases: [Case]
  }

  type UserCart{
    order: [Order]
  }

  type Cart {
    _id: String,
    subTotal: Int,
    items: [Case],
    userId: String,
  }

  type CartDummy {
    _id: String,
    date: String,
    quantity: String,
    price: String,
    status: String
  }

  type Address {
    _id: String,
    userId: String,
    extraAddress: String,
    fullAddress: String,
    zonecode: String,
  }

  type Payment {
    _id: String,
    userId: String,
    accNo: String,
    date: String,
    selected: Boolean
  }

  type Collages {
    id:ID
    paths:[Paths]
  }

  type Paths {
    id:ID
    d:String
  }

  type Cliparts {
    src: String
    id:ID
  }

  type Case {
    caseData: String,
    count: Int,
    price: Int,
    name: String,
    preview: String,
    _id: String,
    brand: String,
    model: String,
    material: String,
    colors: [color],
    theme: String,
    outlineSvgData: String,
    productDetail: String
  }

  input CaseToSend {
    caseData: String,
    price: Int,
    count: Int
    name: String,
    preview: String,
    _id: String,
    brand: String,
    model: String,
    material: String,
    colors: [colorToSend],
    theme: String,
    outlineSvgData: String,
    productDetail: String
  }


  type color {
    _id: Int,
    label: String,
    value: String,
    code: String
  }

input colorToSend {
    _id: Int,
    label: String,
    value: String,
    code: String
}

type colorList {
    list: [color]
}

  type Cases {
    model: String
    brand: String
    transform:String
    height:String
    width:String
    image:String
    shadowImage:String
    path:String
  }

  type Order{
    _id: String,
    status: String,
    userId: String,
    firstName: String,
    lastName: String,
    extraAddress: String,
    fullAddress: String,
    zonecode: String,
    extraDetails: String,
    mobileNumber: String,
    email: String,
    orderNotes: String,
    createdAt: String,
    cartId: String,
    subTotal: String,
    items: [Case],
  }

  type Material {
    title: String
    id:ID
  }

  type Models {
    list: [Model]
  }

  type Model {
    _id: String,
    brand: String,
    price: String,
    model: String,
    transform: String,
    height: String,
    width: String,
    image: String,
    shadowImage: String,
    path: String,
    activePathBack: String,
    activePathMain: String,
    printablePath: String
  }

  # The "Query" type is the root of all GraphQL queries.

  type Query {
    materials: [Material],
    cases: [Cases],
    cliparts: [Cliparts],
    collages: [Collages]
  }
`;

module.exports = mergeGraphqlSchemas.mergeTypes([UserType, caseType, typeDefs, Mutations, Queries], { all: true })