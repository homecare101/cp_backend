const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Order = new Schema({
     status: String,
     userId: String,
     firstName: String,
     lastName: String,
     extraAddress: String,
	fullAddress: String,
	zonecode: String,
	extraDetails: String,
     mobileNumber: String,
     email: String,
     orderNotes: String,
     createdAt: String,
     cartId: String,
     userId: String,
     subTotal: String,
     items: Array,
});

module.exports = Order