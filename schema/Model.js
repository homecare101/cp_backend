const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Model = new Schema({
    brand: String,
    model: String,
    price: Number,
    transform: String,
    height: String,
    width: String,
    image: String,
    shadowImage: String,
    path: String,
    activePathBack: String,
    activePathMain: String,
    printablePath: String
});

module.exports = Model