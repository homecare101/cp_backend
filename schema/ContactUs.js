const mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ContactUs = new Schema({
    email: String,
    name: String,
    message: String
});

module.exports = ContactUs