const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Case = new Schema({
    caseData : String,
    count: Number,
    price: Number,
    name: String,
    userId: String,
    preview: String,
    brand: String,
    model: String,
    material: String,
    outlineSvgData: String,
    productDetail: String
});

module.exports = Case