const UserSchema = require('./User')
const ContactUsSchema = require('./ContactUs')
const CaseSchema = require('./Case')
const AddressSchema = require('./Address')
const PaymentSchema = require('./Payment')
const CartSchema = require('./Cart')
const OrderSchema = require('./Orders')
const StockCasesSchema = require('./StockCases')
const ModelSchema = require('./Model')
module.exports = {
    UserSchema,
    ContactUsSchema,
    CaseSchema,
    AddressSchema,
    PaymentSchema,
    CartSchema,
    OrderSchema,
    StockCasesSchema,
    ModelSchema
}