const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Address = new Schema({
	userId: String,
    extraAddress: String,
	fullAddress: String,
	zonecode: String
});

module.exports = Address