const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var User = new Schema({
    email: String,
    firstName: String,
    lastName: String,
    provider: String,
    providerId: String,
    password: String,
    userName: String,
    gender: String,
    phone: String,
    address: String,
    role: String,
    verificationCode: {
        type: String,
        default: ''
    }
});

module.exports = User