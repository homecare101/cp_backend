const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Payment = new Schema({
	userId: String,
	accNo: String,
	date: String,
	selected: Boolean
});

module.exports = Payment