const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var StockCases = new Schema({
    userId: String,
    count: Number,
    price: Number,
    name: String,
    preview: String,
    brand: String,
    model: String,
    material: String,
    colors: Array,
    theme: String
});

module.exports = StockCases