const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Cart = new Schema({
     subTotal: Number,
     items: Array,
     userId: String,
});

module.exports = Cart