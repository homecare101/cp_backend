const materials = [
    {
        title: 'Hard Case',
        id: 1,
        price:'12,900',
        name:"하드케이스"
    },
    {
        title: 'Tough Case',
        id: 2,
        price:'15,900',
        name:"터프케이스"
    },
    {
        title: 'Slide Case',
        id: 3,
        price:'17,900',
        name:"슬라이드케이스"
    },
    {
        title: 'Gel Hard Case',
        id: 4,
        price:'12,900',
        name:"젤하드케이스"
    },
    {
        title: 'Door Bumper Case',
        id: 5,
        price:'17,900',
        name:"도어범퍼케이스"
    }
]

module.exports = materials