const multer = require('multer');
const { ApolloServer, gql } = require('apollo-server-express');
const https = require('https');
// initilise databse connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/cp');
const fs = require('fs')
const typeDefs = require('./typeDefs')
const MutationResolver = require('./resolvers/Mutation')
const QueryResolver = require('./resolvers/Query')

const cases = require('./json/cases')
const cliparts = require('./json/cliparts')
const collages = require('./json/collages')
const materials = require('./json/materials')

const httpsOptions = {
  key: fs.readFileSync('./localhost.key'),
  cert: fs.readFileSync('./localhost.cert')
}


const express = require('express');
const bodyParser = require('./components/body-parser');



const resolvers = {
  Query: {
    materials: () => materials,
    cases: () => cases,
    cliparts: () => cliparts,
    collages: () => collages,
    ...QueryResolver
  },
  Mutation: MutationResolver
};

const context = ({ req }) => {
};


const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: context,
    playground: {
      endpoint: '/server/playground',
      settings: {
        'editor.theme': 'dark'
      }
    }
});

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

server.applyMiddleware({ app, path: '/server' });

const storage = multer.diskStorage({
    destination: './uploads',
    filename: function (req, file, callback) {
        callback(null, Date.now() + file.originalname) 
    }
});

app.use(bodyParser.json({limit: '1000000000kb', type: 'application/json'}));

app.use(bodyParser.raw({limit: "1000000000kb"}));

app.use(bodyParser.urlencoded({limit: '1000000000kb', extended: true,  parameterLimit:50000 }));


var upload = multer({storage: storage});

app.post('/upload', upload.single('file'), (req, res) => {
    
    if (!req.file) {
      return res.send({
        success: false
      });
  
    } else {
      return res.json({
        filename: req.file.filename
      })
    }

});

app.use('/uploads', express.static(__dirname + '/uploads'));

app.use(express.static(__dirname + '/build'));

app.get('/*', function(req, res){
 res.sendFile('/build/index.html' ,{root:__dirname});
});


const secureServer = https.createServer(httpsOptions, app)

secureServer.listen({ port: 3005 }, () => {
  console.log(`🚀 Server ready at port 3005`);
});



